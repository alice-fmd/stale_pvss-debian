#!/usr/bin/make -f
# debian/rules for alien

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Use v4 compatability mode, so ldconfig gets added to maint scripts.
export DH_COMPAT=4
VERS	= 3.7
RVERS	= $(shell echo $(VERS) | tr -d '.')
REL	= 304
INST	= /opt/pvss/pvss2_v$(VERS)
PACKAGE	= $(shell dh_listpackages)
STUFF	= etm-PVSS_Console-$(VERS).desktop	\
	  etm-PVSS-$(VERS).directory		\
	  etm-PVSS_PA-$(VERS).desktop		\
	  pvssInst.conf				

%-$(VERS).desktop:%.desktop.in
	sed -e 's,@version@,$(VERS),g' \
	    -e 's,/etc/opt/pvss,/etc/pvss,g' \
	    < $< > $@ 

%-$(VERS).directory:%.directory.in
	sed -e 's,@version@,$(VERS),g' \
	    -e 's,/etc/opt/pvss,/etc/pvss,g' \
	    < $< > $@ 

%:%.in
	sed -e 's,@version@,$(VERS),g' \
	    -e 's,/etc/opt/pvss,/etc/pvss,g' \
	    < $< > $@ 

build:
	dh_testdir

clean:
	dh_testdir
	dh_testroot
	dh_clean -d

binary-indep: build
	$(MAKE) -C debian -f rules $(STUFF)

binary-arch: build 
	dh_testdir
	dh_testroot
	dh_clean -k -d
	dh_installdirs

	dh_installdocs
	dh_installchangelogs

# Copy the packages's files.
	find . -maxdepth 1 -mindepth 1 -not -name debian -print0 | \
		xargs -0 -r -i cp -a {} debian/$(PACKAGE)
	rm -f debian/$(PACKAGE)/$(INST)/bin/libXinerama.so.1 
	rm -f debian/$(PACKAGE)/$(INST)/bin/libqt-mt.so.3
	rm -f debian/$(PACKAGE)/$(INST)/bin/sqldrivers/libqsqlmysql.so
	rm -f debian/$(PACKAGE)/$(INST)/bin/sqldrivers/libqsqlodbc.so
	cp debian/etm-PVSS-$(VERS).directory debian/$(PACKAGE)/$(INST)/
	mkdir -p debian/$(PACKAGE)/etc/pvss
	cp debian/pvssInst.conf debian/$(PACKAGE)/etc/pvss/
	sed 's,/etc/opt/pvss,/etc/pvss,' 		\
		< debian/$(PACKAGE)/opt/pvss/startPA 	\
		> debian/$(PACKAGE)/opt/pvss/startPA~
	mv debian/$(PACKAGE)/opt/pvss/startPA~ 		\
	   debian/$(PACKAGE)/opt/pvss/startPA
	chmod 755 debian/$(PACKAGE)/opt/pvss/startPA
	sed -e 's,pvss_path.*=.*,pvss_path = $(INST),'	\
	    -e 's,proj_path.*=.*,proj_path = $(INST),'  \
	    -e 's,langs.*=.*,langs = en_US.iso88591,' 	\
	    < debian/$(PACKAGE)/$(INST)/config/config 	\
	    > debian/$(PACKAGE)/$(INST)/config/config~ 
	mv debian/$(PACKAGE)/$(INST)/config/config~ 	\
	   debian/$(PACKAGE)/$(INST)/config/config
	mkdir -p debian/$(PACKAGE)/usr/share/applications
	cp debian/*.desktop debian/$(PACKAGE)/usr/share/applications
#
# If you need to move files around in debian/$(PACKAGE) or do some
# binary patching, do it here
#


# This has been known to break on some wacky binaries.
#	dh_strip
	dh_compress
	dh_desktop
#	dh_fixperms
	dh_makeshlibs -V
	dh_installdeb
	LD_LIBRARY_PATH=$(shell pwd)/debian/$(PACKAGE)/$(INST)/bin \
	  dh_shlibdeps \
	    $(foreach i, \
		bin/libAGLink40.so.4					\
		bin/ida							\
		bin/console						\
		bin/libManagerV$(RVERS)_$(REL).so			\
		bin/libMessagesV$(RVERS)_$(REL).so			\
		dbdfiles/version_$(VERS)/libManagerV$(RVERS)_$(REL).so	\
		dbdfiles/version_$(VERS)/libMessagesV$(RVERS)_$(REL).so, \
		--exclude=$(i)) 2>/dev/stdout | grep -v libfakeroot-sysv.so
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary
